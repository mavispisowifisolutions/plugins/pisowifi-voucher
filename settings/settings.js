
var path = require('path')

register_options = {
}

form_handler = function( ){
  if ( posted['generate-vouchers']) {
    return new Promise((resolve, reject) => {
      if ( !isNaN(posted['total-vouchers']) ) {
        var vOptions = {
          time: posted['time'], 
          datacap: posted['datacap'] || 0,
        }
        db.voucher.createCodes( posted['total-vouchers'], vOptions, resolve);
      } else {
        resolve({})
      }
    })
    .then(res => { res.restart = false; return res; })
  }
}

data_store = function( next ){
  var data = {};
  Promise.all([
    db.voucher.findAll({raw:true, order: [['id', 'DESC']]})
      .then(result=>{
        data.vouchers = result;
        return result;
      })
  ])
  .then(result=>{ next(data) })
}
