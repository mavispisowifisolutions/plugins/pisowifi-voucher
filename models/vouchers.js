'use strict';
var crypto = require('crypto');

Object.assign(String.prototype, {
  toHHMM() {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    //return hours+':'+minutes+':'+seconds;
    return hours+':'+minutes;
  }
});

Object.assign(String.prototype, {
  toSeconds() { 
    var seconds = 0;
    if (!this) return 0; 
    if (this.indexOf(':')) {
      var hms = this.split(':'); 
      seconds = (+hms[0]) * 60 * 60 + (+hms[1]) * 60;
    } else if ( !isNaN(this) ){
      seconds = parseInt(this,10);
    }
    return seconds; 
  }
})

module.exports = (sequelize, DataTypes) => {
  const vouchers = sequelize.define('voucher', {
    code: {
      type: DataTypes.STRING,
      unique: true,
    },
    status: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    datacap: {
      type: DataTypes.STRING,
      defaultValue: '0',
    },
    time: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    expiry: {
      type: DataTypes.DATE,
      defaultValue: new Date(),
    },
  }, {});
  vouchers.initModel = models => {
    var v = models.voucher;
    var timeout;
    var generateCodes = function(n, codes){
      codes = codes || [];
      var code = crypto.randomBytes(3).toString('hex');
      return v.findAll({raw:true, where: { code: {[models.Sequelize.Op.eq]:code} }})
        .then(res => {
          if (!res.length) {
            codes.push(code)
            clearTimeout( timeout );
            timeout = null;
          } else if ( !timeout ) {
            timeout = setTimeout(()=>{
              n = codes.length
            },5000)
          }
          if ( n == codes.length ) {
            clearTimeout( timeout );
            timeout = null;
            return codes;
          } else {
            return generateCodes(n, codes)
          }
        })
    }
    v.createCodes = ( n, options, next ) => {
      console.log('generating ', n, 'voucher codes...')
      options = options || {};
      if ( isNaN(options.time) ) {
        options.time = options.time.toSeconds();
      }
      if ( !isNaN(n) && !isNaN(options.time) ) {
        var p = options.prefix || '';
        var vCodes = [];
        return generateCodes(n).then( codes => {
          codes.forEach( code => {
            vCodes.push({
              code: p + code,
              datacap: options.datacap || '0',
              time: options.time || 0,
            })
          });
          return v.bulkCreate(vCodes);
        })
        .then(res=>vCodes)
        .then(next);
      }
    }
    /*
    const MINUTE_IN_SECONDS  = 60; //seconds
    const HOUR_IN_SECONDS    = 60 * MINUTE_IN_SECONDS
    const DAY_IN_SECONDS     = 24 * HOUR_IN_SECONDS
    const WEEK_IN_SECONDS    = 7 * DAY_IN_SECONDS
    const MONTH_IN_SECONDS   = 30 * DAY_IN_SECONDS
    const YEAR_IN_SECONDS    = 365 * DAY_IN_SECONDS
    const Megabytes          = 1000000; // bytes

    // sample usage 
    var vOptions = {
      time: 3 * HOUR_IN_SECONDS, // 3 hours
      datacap: 100 * Megabytes, // 100Mb
      
    }
    //db.voucher.createCodes(11, vOptions, console.log);

    // sample output
    /*
    generating  11 voucher codes...
    [ { code: '97b75c', datacap: 100000000, time: 10800 },
      { code: '47e0fd', datacap: 100000000, time: 10800 },
      { code: '4cd17b', datacap: 100000000, time: 10800 },
      { code: 'e9e562', datacap: 100000000, time: 10800 },
      { code: '008c7e', datacap: 100000000, time: 10800 },
      { code: 'e940a8', datacap: 100000000, time: 10800 },
      { code: 'de47cf', datacap: 100000000, time: 10800 },
      { code: '72da81', datacap: 100000000, time: 10800 },
      { code: '58261d', datacap: 100000000, time: 10800 },
      { code: '52eb7c', datacap: 100000000, time: 10800 },
      { code: 'aba6b1', datacap: 100000000, time: 10800 } ]

     */
  };
  
  
  return vouchers;
};